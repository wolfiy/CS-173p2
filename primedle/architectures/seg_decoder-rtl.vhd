architecture rtl of seg_decoder is
begin

    -- GRADED AND PASSED
    -- converts two bits into the corresponding sequence to light up the
    -- seven segments display
    display:process (digit) is
    begin 
        case digit is
            when "00" => seg <= "11111100";
            when "01" => seg <= "01100000";
            when "10" => seg <= "11011010";
            when "11" => seg <= "11110010";
            when others => seg <= "11111111";
        end case;
    end process display;

end architecture rtl;