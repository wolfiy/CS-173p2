architecture rtl of game_ctrl is
-- FSM states
type state_type is (idle, check, update, fin);
signal state : state_type;

-- signals
signal s_trials_count : std_logic_vector(1 to 30);
signal s_hint : hint_t;
signal s_finished, s_rejected, s_led : std_logic;
signal s_game_records : history_t;
-- max 5 hints -> 2^3 = 8 is enough
signal s_counter : unsigned(2 downto 0);
signal s_counter_old : unsigned(2 downto 0);
--signal s_counter : unsigned(2 downto 0);

-- colors
constant white : color_t := "00";
constant black : color_t := "01";
constant yellow : color_t := "10";
constant green : color_t := "11"; 

-- debug
signal s_a, s_b, s_c : std_logic;

-- GOLDEN
-- golden is stable (after first check ~ before reset)

-- READY
-- waiting: ready = 1 and keep data
-- check pressed -> ready = 0 (at least one clock) and keep data
-- ready = 1 => all inputs prepared and update game records

-- GUESS
-- guess invalid: add one led in trial count AND reject led = 1
-- guess valid: add one led in trial count AND reject led = 0
--              compare  guess & golden AND add row of hint in game_records
--              game finished?
--                  yes -> dead state, keeps data, no updates til reset (i.e. found or 5 rows of hint given)
--                  no -> nothing

-- UPDATE
-- can take more than 1 clock
-- led_en = 0

begin

    finished <= s_finished;
    rejected <= s_rejected;
    LED_en <= s_led;
    trials_count <= s_trials_count;
    game_records <= s_game_records;

    task4:process (clk) is
    begin

    if (rising_edge(clk)) then
        -- reset goes back to idle and prepare for a game
        if (reset = '1') then -- reset
            state <= idle;
            
            s_game_records <= (others => (others => (others => '0')));
            s_trials_count <= (others => '0');
            s_finished <= '0';
            s_rejected <= '0';
            s_led <= '0';

            -- hints counter at 0
            s_counter <= to_unsigned(0, 3);
            s_counter_old <= to_unsigned(0, 3);

            s_a <= '0';
            s_b <= '0';

            -- initially, the hint history is empty

        else -- use fsm
            -- idle state
            if (state = idle) then
                -- correct values are already assigned
                s_led <= '1';
            end if;

            -- check state
            if (state = check) then
                s_led <= '0';
            end if;

            -- update state
            if (state = update) then
            -- TODO trial count might need to go lower
                s_led <= '1';
                --s_trials_count <= s_trials_count ;
                s_trials_count <= '1' & s_trials_count(1 to 29);

                s_hint(0) <= white;
                s_hint(1) <= white;
                s_hint(2) <= white;
                s_hint(3) <= white;

                -- the guess is invalid
                if (is_prime = '0') then
                    s_rejected <= '1';
                else
                    s_rejected <= '0';
                    if (unsigned(guess) = unsigned(golden)) then
                        s_game_records(to_integer(s_counter))(0) <= green; 
                        s_game_records(to_integer(s_counter))(1) <= green; 
                        s_game_records(to_integer(s_counter))(2) <= green; 
                        s_game_records(to_integer(s_counter))(3) <= green; 
                        s_finished <= '1';
                    else 
                        -- 7 6 5 4 3 2 1 0
                        -- put the hints at white, they will change after if needed
                        -- giving yellow
                        s_counter_old <= s_counter;
                        -- s_game_records(to_integer(s_counter_old))(0) <= black;
                        -- s_game_records(to_integer(s_counter_old))(1) <= black;
                        -- s_game_records(to_integer(s_counter_old))(2) <= black;
                        -- s_game_records(to_integer(s_counter_old))(3) <= black;
                        

                        if (guess(7 downto 6) = golden(7 downto 6)) then 
                            s_game_records(to_integer(s_counter_old))(3) <= green;
                        elsif (guess(7 downto 6) = golden(1 downto 0) 
                            or guess(7 downto 6) = golden(3 downto 2) 
                            or guess(7 downto 6) = golden(5 downto 4)) 
                        then
                            s_game_records(to_integer(s_counter_old))(3) <= yellow;
                        else
                            s_game_records(to_integer(s_counter_old))(3) <= black;
                        end if;

                        if (guess(5 downto 4) = golden(1 downto 0) 
                            or guess(5 downto 4) = golden(3 downto 2) 
                            or guess(5 downto 4) = golden(7 downto 6)) 
                        then
                            s_game_records(to_integer(s_counter_old))(2) <= yellow;
                        elsif (guess(5 downto 4) = golden(5 downto 4)) then 
                            s_game_records(to_integer(s_counter_old))(2) <= green;
                        else
                            s_game_records(to_integer(s_counter_old))(2) <= black;
                        end if;

                        if (guess(3 downto 2) = golden(1 downto 0) 
                            or guess(3 downto 2) = golden(5 downto 4) 
                            or guess(3 downto 2) = golden(7 downto 6)) 
                        then 
                            s_game_records(to_integer(s_counter_old))(1) <= yellow; 
                        elsif (guess(3 downto 2) = golden(3 downto 2)) then 
                            s_game_records(to_integer(s_counter_old))(1) <= green; 
                        else
                            s_game_records(to_integer(s_counter_old))(1) <= black;
                        end if;

                        if (guess(1 downto 0) = golden(3 downto 2) 
                            or guess(1 downto 0) = golden(5 downto 4) 
                            or guess(1 downto 0) = golden(7 downto 6)) 
                        then
                            s_game_records(to_integer(s_counter_old))(0) <= yellow; 
                        elsif (guess(1 downto 0) = golden(1 downto 0)) then 
                            s_game_records(to_integer(s_counter_old))(0) <= green; 
                        else
                            s_game_records(to_integer(s_counter_old))(0) <= black;
                        end if;

                        s_counter <= s_counter + 1;

                        -- old version
                        -- if (guess(1 downto 0) = golden(3 downto 2) 
                        --     or guess(1 downto 0) = golden(5 downto 4) 
                        --     or guess(1 downto 0) = golden(7 downto 6)) 
                        --     then s_hint(0) <= yellow; 
                        --          s_counter <= s_counter + 1;
                        --          s_game_records(to_integer(s_counter_old))(0) <= yellow; 
                        -- end if;
                        -- if (guess(3 downto 2) = golden(1 downto 0) 
                        --     or guess(3 downto 2) = golden(5 downto 4) 
                        --     or guess(3 downto 2) = golden(7 downto 6)) 
                        --     then s_hint(1) <= yellow;
                        --          s_counter <= s_counter + 1;
                        --          s_game_records(to_integer(s_counter_old))(1) <= yellow; 
                        -- end if;
                        -- if (guess(5 downto 4) = golden(1 downto 0) 
                        --     or guess(5 downto 4) = golden(3 downto 2) 
                        --     or guess(5 downto 4) = golden(7 downto 6)) 
                        --     then s_hint(2) <= yellow;
                        --         --game_records(s_counter)(0) <= "11";
                        --          s_counter <= s_counter + 1;
                        --          s_game_records(to_integer(s_counter_old))(2) <= yellow;
                        -- end if;
                        -- if (guess(7 downto 6) = golden(1 downto 0) 
                        --     or guess(7 downto 6) = golden(3 downto 2) 
                        --     or guess(7 downto 6) = golden(5 downto 4)) 
                        --     then s_hint(3) <= yellow;
                        --          s_counter <= s_counter + 1;
                        --          s_game_records(to_integer(s_counter_old))(3) <= yellow;
                        -- end if;

                        -- giving green
                        -- if (guess(1 downto 0) = golden(1 downto 0)) then 
                        --     s_hint(0) <= green;
                        --     s_counter <= s_counter + 1;
                        --     s_game_records(to_integer(s_counter_old))(0) <= green; 
                        -- end if;
                        -- if (guess(3 downto 2) = golden(3 downto 2)) then 
                        --     s_hint(1) <= green;
                        --     s_counter <= s_counter + 1;
                        --     s_game_records(to_integer(s_counter_old))(1) <= green; 
                        -- end if;
                        -- if (guess(5 downto 4) = golden(5 downto 4)) then 
                        --     s_hint(2) <= green;
                        --     s_counter <= s_counter + 1;
                        --     s_game_records(to_integer(s_counter_old))(2) <= green;
                        -- end if;
                        -- if (guess(7 downto 6) = golden(7 downto 6)) then 
                        --     s_hint(3) <= green;
                        --     s_counter <= s_counter + 1;
                        --     s_game_records(to_integer(s_counter_old))(3) <= green;
                        -- end if;
                        
                        --s_c <= '1';

                        --s_game_records(to_integer(s_counter_old))(0) <= s_hint(0); 
                        --s_game_records(to_integer(s_counter_old))(1) <= s_hint(1); 
                        --s_game_records(to_integer(s_counter_old))(2) <= s_hint(2); 
                        --s_game_records(to_integer(s_counter_old))(3) <= s_hint(3); 
                    end if;
                end if;

                -- game ended
                if (s_counter >= to_unsigned(5, 3)) then
                    s_finished <= '1';
                    state <= fin;
                end if;
            end if;

            -- fin state
            if (state = fin) then

            end if;


            -- transition logic
            case state is
                when idle => if (ready = '1') then state <= idle;
                                else state <= check;
                                end if;

                when check => if (ready = '1') then state <= update;
                                else state <= check;
                                end if;

                when update => if (s_finished = '1') then state <= fin;
                                else state <= idle;
                                end if;

                when fin => if (reset = '1') then state <= idle;
                            else state <= fin;
                            end if;
            end case;
        end if;
    end if;
    end process task4;

end architecture rtl;