# TEST STEP 3 (prime_checker)

quit -sim
vsim -t ps prime_checker

# INPUTS
add wave -divider Inputs:
add wave -color "hot pink" -in *

# OUTPUTS
add wave -divider Outputs: 
add wave -color "sky blue" -out *

# SIGNALS 
add wave -divider Signals:
add wave -color "gold" -internal * 

radix signal guess unsigned

force clk   0 00, 1 05 -r 10
force reset 0 0, 1 7, 0 12 

force check 0 0, 1 25
force check 0 400, 1 405
force guess 11111010 0
# force guess 00001010 400
#0 00000000 
#1 00000001
#2 00000010
#5 00000101
#6 00000110
#241 11110001
#251 11111011
#252 11111100

run 300
# 7680
wave zoom full

echo "Wave Simulation Generated"
