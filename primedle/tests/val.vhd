architecture rtl of prime_checker is
    type State_type is (WAITING, DO);
    signal State      : State_type;
    signal next_state : State_type;
    signal s_shift : std_logic;
    signal s_ready : std_logic;
begin

    ready <= s_ready;

    -- en un seul process avec un shift
    v2:process (clk, State, guess, s_shift) is
    begin
        if rising_edge(clk) then
            if (reset = '1') then
                is_prime <= '1';
                s_ready    <= '1';
                State    <= WAITING;
            elsif (check = '1') then
                s_ready <= '0';
                s_shift <= '0';
                State <= next_state;
            end if;
        end if;

        case State is
            when WAITING =>
                s_ready <= s_shift;
                next_state <= DO;
            when DO =>
                ready <= '0';
                for i in 0 to primes_logic_vector'length - 1 loop
                    if (guess = primes_logic_vector(i)) then
                        is_prime <= '1';
                        exit;
                    end if;
                end loop;
                s_shift <= '1';
        end case;
    end process v2;

    -- ta version
    process(clk)
    begin
        if rising_edge(clk) then
            if (reset = '1') then
                is_prime <= '1';
                ready    <= '1';
                State    <= WAITING;
            elsif (check = '1') then
                State <= next_state;
            end if;
        end if;
    end process;

    process(guess, State) is
    begin
        case State is
            when WAITING =>
                next_state <= DO;
            when DO =>
                ready <= '0';
                --idee de base
                --with guess select
                --is_prime <= '1' when x"0"
                --ect. . .
                for i in 0 to primes_logic_vector'length - 1 loop
                    if (guess = primes_logic_vector(i)) then
                        is_prime <= '1';
                        exit;
                    end if;
                end loop;
                ready <= '1';
                --            when WAITING =>
                --                ready      <= '0';
                --                is_prime <= '0';
                --                --idee de base
                --                --with guess select
                --                --is_prime <= '1' when x"0"
                --                --ect. . .
                --                for i in 0 to primes_logic_vector'length - 1 loop
                --                    if (guess = primes_logic_vector(i)) then
                --                        is_prime <= '1';
                --                        exit;
                --                    end if;
                --                end loop;
                --                next_state <= DO;
                --            when DO =>
                --                ready <= '1';

        end case;
    end process;
end architecture rtl;