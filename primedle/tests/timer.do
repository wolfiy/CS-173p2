#  _   _
# | |_(_)_ __ ___   ___ _ __
# | __| | '_ ` _ \ / _ | '__|
# | |_| | | | | | |  __| |
#  \__|_|_| |_| |_|\___|_|

quit -sim
vsim -t ps timer

# inputs
add wave -divider Inputs:
add wave -color "hot pink" -in *

# outputs
add wave -divider Outputs: 
add wave -color "sky blue" -out *

# internal signals
add wave -divider Signals:
add wave -color "gold" -internal * 

# add wave -position insertpoint \
# sim:/timer/*

# formatting
radix signal s_counter decimal

# force
force clk 0 0, 1 5 -r 10
force clear 0 0, 1 5, 0 10, 1 50, 0 60, 1 1700000, 0 1800000

# run
run 4000000
wave zoom full

echo "simulation done"