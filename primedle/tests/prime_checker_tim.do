# TEST STEP 3 (prime_checker)
# (edited)

quit -sim
vsim -t ps prime_checker

# INPUTS
add wave -divider Inputs:
add wave -color "hot pink" -in *

# OUTPUTS
add wave -divider Outputs: 
add wave -color "sky blue" -out *

# SIGNALS 
add wave -divider Signals:
add wave -color "gold" -internal * 

radix signal guess unsigned

force clk   0 0, 1 5 -r 10
force reset 1 0, 0 10 
force check 0 0, 1 10, 0 20 -r 30
force guess 00000000 0
force guess 00000000 10
force guess 00000001 40
force guess 00000010 70
force guess 00000011 100
force guess 00000100 130
force guess 00000101 160
force guess 00000110 190
force guess 00000111 220
force guess 00001000 250
force guess 00001001 280
force guess 01100101 310
force guess 00001010 340
force guess 10101011 370
force guess 11001000 400
force guess 11110001 430
force guess 11111011 470
force guess 00000100 500

run 530
# 7680
wave zoom full

echo "Wave Simulation Generated"
