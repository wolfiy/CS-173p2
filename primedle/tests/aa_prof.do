vsim -t ns game_ctrl;
add wave *

#radix signal state state_type

force clk 0 0, 1 5 -repeat 10;

#1
force reset 1 2;
force guess 00000000 2;
force golden 00000000 2;
force ready 1 12;
force is_prime 0 12;


#2
force reset 0 12;
force guess 00000000 112;
force golden 00000000 12;
force ready 1 12;
force is_prime 0 12;


#3
force reset 0 22;
force guess 00010001 22;
force golden 00010001 22;
force ready 1 12;
force is_prime 0 12;


#4
force reset 0 32;
force guess 00010001 32;
force golden 00010001 32;
force ready 1 12;
force is_prime 0 12;


#5
force reset 0 42;
force guess 00010001 42;
force golden 00010001 42;
force ready 0 42;
force is_prime 0 42;


#6
force reset 0 52;
force guess 00010001 52;
force golden 00010001 52;
force ready 0 52;
force is_prime 1 52;


#7
force reset 0 62;
force guess 00010001 62;
force golden 00010001 62;
force ready 1 62;
force is_prime 1 62;


#8
force reset 0 72;
force guess 00010001 72;
force golden 00010001 72;
force ready 1 72;
force is_prime 1 72;


#9
force reset 0 82;
force guess 00010001 82;
force golden 00010001 82;
force ready 1 82;
force is_prime 1 82;



 run 100 
