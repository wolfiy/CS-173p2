vsim -t ns game_ctrl
add wave *
force clk 0 0 , 1 5, 0 15 -repeat 20
force reset 1 0, 0 20, 1 300, 0 320
force guess 00000000 0, 00010001 40, 00000101 340, 00011111 540
force golden 00000000 0, 00010001 20, 00111011 320
force ready 1 0, 0 80, 1 120, 0 380, 1 420, 0 560, 1 620
force is_prime 0 0, 1 100
run 700
