#      _      _
#   __| | ___| |__   ___  _   _ _ __   ___ ___ _ __
#  / _` |/ _ | '_ \ / _ \| | | | '_ \ / __/ _ | '__|
# | (_| |  __| |_) | (_) | |_| | | | | (_|  __| |
#  \__,_|\___|_.__/ \___/ \__,_|_| |_|\___\___|_|

quit -sim
vsim -t ps debouncer

# inputs
add wave -divider Inputs:
add wave -color "hot pink" -in *

# outputs
add wave -divider Outputs: 
add wave -color "sky blue" -out *

# internal signals
add wave -divider Signals:
add wave -color "gold" -internal * 

# add wave -position insertpoint \
# sim:/debouncer/*

# force
force clk 0 0, 1 5 -r 10
force clear 0 0, 1 10, 0 20, 1 150, 0 155
force button 0 0, 1 30, 0 40, 1 250, 0 255

# run
run 600
wave zoom full