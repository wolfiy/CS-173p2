vsim -t ns game_ctrl;
add wave *

# internal signals
add wave -divider Signals:
add wave -color "gold" -internal * 


#radix signal state state_type

 force clk 0 0, 1 5 -repeat 10;
 force reset 1 0, 0 20;
 # force guess  00011001;
 force guess  00011011;
 force golden 00011001;
force is_prime 1 20;
force reset 1 10, 0 15
 force ready 1 0, 0 50, 1 100, 0 130, 1 160, 0 190, 1 220, 0 310, 1 340, 0 370, 1 400, 0 420, 1 440, 0 460, 1 480, 0 500, 1 520, 0 540, 1 560, 0 580, 1 600, 0 620, 1 640, 0 660, 1 680, 0 700, 1 720, 0 740, 1 760, 0 780, 1 800, 0 820, 1 840, 0 860, 1 880, 0 900, 1 920, 0 940, 1 960, 0 980, 1 1000, 0 1020, 1 1040, 0 1060, 1 1080, 0 1100, 1 1120, 0 1140, 1 1160, 0 1180, 1 1200, 0 1220, 1 1240, 0 1260, 1 1280, 0 1300, 1 1320, 0 1340, 1 1360, 0 1380, 1 1400, 0 1420, 1 1440, 0 1460, 1 1480, 0 1500, 1 1520, 0 1540, 1 1560;
 force is_prime 0; run 1700 
# 1800 avant