architecture rtl of prime_checker is

-- signals to start checking
signal s_is_prime : std_logic;
-- counter used for the array. it won't exceed 54, thus the 6 bits.
-- signal s_index, s_next : natural;
-- used to represent the current prime used for comparison.
-- signal s_current_prime : std_logic_vector(7 DOWNTO 0);
-- the user input (the guess taking place)
-- signal s_input : std_logic_vector(7 DOWNTO 0);
-- signal to start (wait one clock)

signal s_ready, s_ready_shift : std_logic;

-- two states, check and not check
-- 54 etats
-- quand dans check voir si c'est dans liste avec lindex i, revenir next state

-- GRADED AND PASSED

begin

ready <= s_ready;

    process (clk) is
    begin
        if (rising_edge(clk)) then
            -- reset if pressed
            if (reset = '1') then
                is_prime <= '1';
                --ready <= '1';
                s_ready <= '1';
            else
                -- todo normal
                if (check = '1') then
                    s_ready <= '0';
                    s_ready_shift <= '0';
                else 
                    s_ready <= s_ready_shift;
                    for i in 0 to primes_logic_vector'length - 1 loop
                        if (unsigned(primes_logic_vector(i)) = unsigned(guess)) then
                            is_prime <= '1';
                            s_ready_shift <= '1';
                            exit;
                        else
                            is_prime <= '0';
                            s_ready_shift <= '0';

                            if (i = primes_logic_vector'length - 1 and s_ready_shift = '0') then
                                s_ready_shift <= '1';
                            end if;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;



    --             if (check = '1') then
    --                 ready <= '0';
    --                 for i in 0 to primes_logic_vector'length - 1 loop
    --                     if (unsigned(primes_logic_vector(i)) = unsigned(guess)) then 
    --                         is_prime <= '1';
    --                         s_ready <= '1';
    --                         exit;
    --                         -- maybe changer le if ici
    --                     elsif (i = primes_logic_vector'length - 1) then
    --                         s_ready <= '1';
    --                     else 
    --                         is_prime <= '0';
    --                         s_ready <= '0';
    --                         -- TODO le ready, pourquoi ça marche pas?
    --                     end if;
    --                 end loop;
    --             end if;
    --         end if;
    --     end if;
    -- end process;

    -- process (clk) is
    -- begin
    --     if (rising_edge(clk)) then
    --         if (check = '1') then
    --             if (reset = '0') then 
    --                 ready <= '0';
    --             end if;
    --         else 
    --             ready <= s_ready;
    --         end if;
    --     end if;
    -- end process;

    --     if (rising_edge(reset)) then 
    --         is_prime <= '1';
    --         ready <= '1';
    --     -- s_input <= guess;
    --     end if;

    --     if (rising_edge(clk) and check = '1') then
    --         ready <= '0';
    --         for i in 0 to primes_logic_vector'length - 1 loop
    --             if (unsigned(primes_logic_vector(i)) = unsigned(guess)) then 
    --                 is_prime <= '1';
    --                 ready <= '1';
    --                 a <= '1';
    --                 exit;
    --             elsif (i = primes_logic_vector'length - 1) then
    --                 ready <= '1';
    --                 a <= '1';
    --             else 
    --                 is_prime <= '0';
    --                 a <= '0';
    --                 ready <= '0';
    --                 -- TODO le ready, pourquoi ça marche pas?
    --             end if;
    --         end loop;
    --     end if;
    --end process;

    -- Tentative mtn: pas de s_ready mais ready directement
    -- ready <= s_ready;
end architecture rtl;
