architecture rtl of timer is

-- counter that will go up to 120m. 32-bits as said on the timer-entity file
signal s_counter : unsigned(31 DOWNTO 0);
signal s_done : std_logic;


-- GRADED AND PASSED
begin
    -- output signal
    done <= s_done;

    -- all in one
    process (clk) is
    begin
        if (rising_edge(clk)) then
            if (clear = '1') then
                s_counter <= to_unsigned(1, 32);
                s_done <= '0';
            end if;
        end if;
            
        if (rising_edge(clk)) then
            if ((clear = '0')) then
                if (s_counter < to_unsigned(FCLK, 32)) then 
                    s_counter <= s_counter + 1;
                else
                    s_done <= '1';

                end if;
            end if;
        end if;
    end process;

    -- TODO pourquoi ça fonctionne pas si c'est séparé de l'autre processe.
    -- process (clk) is
    -- begin
    --     if (rising_edge(clk)) then
    --        if (clear = '1') then
    --             s_counter <= to_unsigned(1, 32);
    --             s_done <= '0';
    --         end if;
    --     end if;
    -- end process;

    ---- old version ----
    -- adds one to the counter on each clock pass
    -- check:process (clk) is
    -- begin 
    --     if (rising_edge(clk) and clear = '0') then
    --         if (s_counter >= to_unsigned(FCLK, 32)) then s_done <= '1';
    --         else s_counter <= s_counter + 1;
    --              s_done <= '0';
    --         end if;
    --     end if;
    -- end process check;

    -- -- puts the counter back to 0
    -- clearing:process (clear) is
    -- begin 
    --     if (clear = '1') then s_counter <= to_unsigned(0, 32);
    --     end if;
    -- end process clearing;
end architecture rtl;
