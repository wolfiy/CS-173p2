architecture rtl of debouncer is

-- signals for the first three flipflops
signal s_butAndNotClear, s_0AndNotClear, s_1AndNotClear, s_1AndNot2 : std_logic;

-- signals for the last flipflop
signal s_3OrAll, s_allAndNotClear : std_logic;

-- signals for the dff outputs
signal s_q0, s_q1, s_q2, s_q3 : std_logic;

-- GRADED AND PASSED
begin
    -- signals after going through the flipflops
    s_butAndNotClear <= button and (not clear);
    s_0AndNotClear <= s_q0 and (not clear);
    s_1AndNotClear <= s_q1 and (not clear);

    -- signals before entering the last flipflop
    s_1AndNot2 <= s_q1 and (not s_q2);
    s_3OrAll <= s_1AndNot2 or s_q3;
    s_allAndNotClear <= s_3OrAll and (not clear);

    -- output signal
    button_o <= s_q3;

    -- all in one variant
    process (clk) is
    begin
        if (rising_edge(clk)) then
            s_q0 <= s_butAndNotClear;
            s_q1 <= s_0AndNotClear;
            s_q2 <= s_1AndNotClear;
            s_q3 <= s_allAndNotClear;
        end if;
    end process;

    -- -- first flipflop (0 on my notes)
    -- process (clk) is
    --     begin if(rising_edge(clk)) then s_q0 <= s_butAndNotClear;
    --     end if;
    -- end process;

    -- -- second flipflop (1 on my notes)
    -- process (clk) is
    --     begin if(rising_edge(clk)) then s_q1 <= s_0AndNotClear;
    --     end if;
    -- end process;

    -- -- third flipflop (2 on my notes)
    -- process (clk) is
    --     begin if(rising_edge(clk)) then s_q2 <= s_1AndNotClear;
    --     end if;
    -- end process;

    -- -- last flipflop (3 on my notes)
    -- process (clk) is
    --     begin if(rising_edge(clk)) then s_q3 <= s_allAndNotClear;
    --     end if;
    -- end process;
end architecture rtl; 
